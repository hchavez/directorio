﻿using System;
using System.Runtime.Caching;
using Mosses.Travertino.Common.Cache;

namespace Mosses.Travertino.Managers.Cache
{
    public class CacheProvider : ICacheProvider
    {
        private ObjectCache Cache => MemoryCache.Default;

        public T Get<T>(string key) where T : class
        {
            return Cache[key] as T;
        }

        public void Set<T>(string key, T data, int cacheTime)
        {
            CacheItemPolicy policy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now + TimeSpan.FromSeconds(cacheTime)
            };

            Cache.Add(new CacheItem(key, data), policy);
        }

        public bool IsSet(string key)
        {
            return Cache[key] != null;
        }

        public void Clear(string key)
        {
            Cache.Remove(key);
        }
    }
}
