﻿using System.Collections.Generic;
using Mosses.Travertino.Common.Cache;
using Mosses.Travertino.Common.Managers;
using Mosses.Travertino.Common.Models;
using Mosses.Travertino.Common.Repositories;

namespace Mosses.Travertino.Managers
{
    public class DirectorioManager : IDirectorioManager
    {
        private IDirectorioRepository DirectorioRepository { get; set; }
        private ICacheProvider CacheProvider { get; set; }

        public DirectorioManager(IDirectorioRepository directorioRepository, ICacheProvider cacheProvider)
        {
            DirectorioRepository = directorioRepository;
            CacheProvider = cacheProvider;
        }

        public IEnumerable<Resultado> Buscar(BusquedaParams busquedaParams)
        {
            return DirectorioRepository.Buscar(busquedaParams);
        }
        
        public Resultado Obtener(string companiaId)
        {
            return DirectorioRepository.Obtener(companiaId);
        }

        public IEnumerable<string> ObtenerSugerencias(string inidicio)
        {
            return DirectorioRepository.ObtenerSugerencias(inidicio);
        }

        public IEnumerable<string> ObtenerCategorias()
        {
            return DirectorioRepository.ObtenerCategorias();
        }
        
        public IEnumerable<Ubicacion> ObtenerUbicaciones(string indicio)
        {
            return DirectorioRepository.ObtenerUbicaciones(indicio);
        }
    }
}
