﻿using System.Collections.Generic;
using Mosses.Travertino.Common.Models;

namespace Mosses.Travertino.Common.Repositories
{
    public interface IDirectorioRepository
    {
        IEnumerable<Resultado> Buscar(BusquedaParams busquedaParams);
        Resultado Obtener(string companiaId);
        IEnumerable<string> ObtenerSugerencias(string inidicio);
        IEnumerable<string> ObtenerCategorias();
        IEnumerable<Ubicacion> ObtenerUbicaciones(string indicio);
    }
}