﻿namespace Mosses.Travertino.Common.Cache
{
    public interface ICacheProvider
    {
        void Set<T>(string key, T data, int cacheTime);
        bool IsSet(string key);
        T Get<T>(string key) where T : class;
        void Clear(string key);
    }
}