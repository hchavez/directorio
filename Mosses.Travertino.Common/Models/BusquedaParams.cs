﻿using System.Collections.Generic;

namespace Mosses.Travertino.Common.Models
{
    public class BusquedaParams
    {
        public BusquedaParams(string termino, string ubicacion)
        {
            Termino = termino;
            Ubicacion = ubicacion;
        }

        public string Termino { get; set; }
        public string Ubicacion { get; set; }
    }
}