﻿using System.Collections.Generic;

namespace Mosses.Travertino.Common.Models
{
    public class BaseModel
    {
        public string Id { get; set; }
    }

    public class Resultado : BaseModel
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
        public string Horario { get; set; }
        public string ImagenUrl { get; set; }
        public string Categoria { get; set; }
        public Coordenadas Coordenadas { get; set; }
        public List<string> Telefonos { get; set; }
        public string TelefonosDescripcion => Telefonos != null ? string.Join("<br/> ", Telefonos) : null;
        public string Email { get; set; }
        public string SitioWeb { get; set; }
        public string AcercaDe { get; set; }
        public List<Servicio> Servicios { get; set; }
        public List<Testimonio> Testimonios { get; set; }
    }

    public class Coordenadas
    {
        public Coordenadas(double latitud, double longitud)
        {
            Latitud = latitud;
            Longitud = longitud;
        }

        public double Latitud { get; set; }
        public double Longitud { get; set; }
    }

    public class Ubicacion : BaseModel
    {
        public string Estado { get; set; }
        public string Ciudad { get; set; }
        public string Abreviatura { get; set; }
        public string Nombre => Ciudad + ", " + Abreviatura;
    }

    public class Servicio : BaseModel
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string ImagenUrl { get; set; }
    }

    public class Testimonio : BaseModel
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string ImagenUrl { get; set; }
    }
}
