﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using Mosses.Travertino.Common.Models;
using Mosses.Travertino.Common.Repositories;

namespace Mosses.Travertino.Data.Mock
{
    public static class MockDirectorioRepository
    {
        public static IDirectorioRepository ObtenerInstancia()
        {
            var mock = new Mock<IDirectorioRepository>();
            mock.Setup(r => r.Buscar(It.IsAny<BusquedaParams>())).Returns((BusquedaParams busquedaParams) =>
            {
                string termino = busquedaParams.Termino.ToLower();
                var resultados = Resultados().ToList();
                resultados = resultados.Where(x =>
                        x.Nombre.ToLower().Contains(termino) || x.Categoria.ToLower().Contains(termino) ||
                        termino.Contains(x.Nombre) || termino.Contains(x.Categoria.ToLower()))
                    .ToList();

                return resultados;
            });

            mock.Setup(r => r.Obtener(It.IsAny<string>())).Returns((string companiaId) =>
            {
                return Resultados().FirstOrDefault(x => x.Id == companiaId);
            });

            mock.Setup(r => r.ObtenerSugerencias(It.IsAny<string>())).Returns((string indicio) =>
            {
                return Resultados().Where(x => x.Nombre.ToLower().Contains(indicio.ToLower())
                                               || indicio.ToLower().Contains(x.Nombre.ToLower())).Select(x => x.Nombre);
            });

            mock.Setup(r => r.ObtenerCategorias()).Returns(Categorias());

            mock.Setup(r => r.ObtenerUbicaciones(It.IsAny<string>())).Returns((string indicio) =>
                {
                    return Ubicaciones().Where(x => x.Nombre.ToLower().Contains(indicio.ToLower()) || indicio.ToLower().Contains(x.Nombre.ToLower()));
                });

            return mock.Object;
        }

        public static IEnumerable<Resultado> Resultados()
        {
            return new List<Resultado>()
            {
                new Resultado()
                {
                    Id = "1",
                    Nombre = "Albañiles Unidos",
                    Descripcion = "Servicios Especializados De Construcción.",
                    AcercaDe = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis vestibulum sem, sit amet suscipit lacus. Proin tempus volutpat quam sed commodo. Aliquam quis euismod risus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam est libero, semper ut tempus ac, dapibus vitae tellus. Nulla faucibus maximus eleifend. Suspendisse ut ipsum at massa viverra congue nec vitae magna. Maecenas at felis justo. Donec nec lorem eget mi ultrices posuere. Vivamus cursus tristique libero sed aliquet. Suspendisse volutpat odio id sapien lobortis, id luctus sem maximus. Suspendisse consectetur augue id tristique finibus. Suspendisse ac nunc metus. In odio justo, porttitor ac pharetra rutrum, tristique nec tortor. Aliquam maximus, sem nec tincidunt lacinia, lacus tellus mattis dui, id dignissim felis magna vel nisl. Etiam et diam et nisi efficitur mattis.",
                    Direccion = "Adelaida Chávez #33. Col. Mayagoitia. Lerdo, Dgo. Lerdo, DGO",
                    Horario = "Lunes a Viernes: 8:00am - 8:00pm <br/> Sábados: 10:00am - 2:00pm",
                    ImagenUrl = "/Content/Images/demo-construccion-2.jpg",
                    Categoria = "Construcción",
                    Coordenadas = new Coordenadas(25.554529, -103.440177),
                    Email = "albañilesunidos@almazones.org",
                    Telefonos = new List<string>()
                    {
                        "8717010101",
                        "8717010102",
                    },
                    SitioWeb = "www.almazones.org",
                    Servicios = new List<Servicio>()
                    {
                        new Servicio()
                        {
                            Id = "1",
                            Nombre = "Construcción de Pirámides",
                            Descripcion = "Construcción a la medida de pirámides que durarán por milenios.",
                        },
                        new Servicio()
                        {
                            Id = "2",
                            Nombre = "Construcción de Murallas",
                            Descripcion = "Construcción de murallas para defensa de invasiones."
                        },
                        new Servicio()
                        {
                            Id = "3",
                            Nombre = "Instalación de Azulejos",
                            Descripcion = "Adorne su palacio con los mejores azulejos traídos desde tierras lejanas."
                        }
                    },
                    Testimonios = new List<Testimonio>()
                    {
                        new Testimonio()
                        {
                            Id = "1",
                            Nombre = "C.P. Fulano Sánchez",
                            Descripcion = "Gracias a su ayuda en la construcción ahora seré recordado por miles de años.",
                            ImagenUrl = "/Content/Images/persona-1.png"
                        },
                        new Testimonio()
                        {
                            Id = "2",
                            Nombre = "Lic. Sutana Gómez",
                            Descripcion = "Ahora mi pueblo puede prosperar sin temor constante a las invasiones bárbaras.",
                            ImagenUrl = "/Content/Images/persona-2.png"
                        }
                    }
                },
                new Resultado()
                {
                    Id = "2",
                    Nombre = "Construcciones Especializadas",
                    Descripcion = "Servicios de construcción.<br />" +
                                  "Instalación de pisos y azulejos.<br />" +
                                  "Instalaciones de plomería y electricidad.<br />" +
                                  "Servicios de soldadura.<br />" +
                                  "Instalación de refrigeración (minisplits, aire acondicionado).",
                    AcercaDe = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis vestibulum sem, sit amet suscipit lacus. Proin tempus volutpat quam sed commodo. Aliquam quis euismod risus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam est libero, semper ut tempus ac, dapibus vitae tellus. Nulla faucibus maximus eleifend. Suspendisse ut ipsum at massa viverra congue nec vitae magna. Maecenas at felis justo. Donec nec lorem eget mi ultrices posuere. Vivamus cursus tristique libero sed aliquet. Suspendisse volutpat odio id sapien lobortis, id luctus sem maximus. Suspendisse consectetur augue id tristique finibus. Suspendisse ac nunc metus. In odio justo, porttitor ac pharetra rutrum, tristique nec tortor. Aliquam maximus, sem nec tincidunt lacinia, lacus tellus mattis dui, id dignissim felis magna vel nisl. Etiam et diam et nisi efficitur mattis.",
                    Direccion = "Adelaida Chávez #33. Col. Mayagoitia. Lerdo, Dgo.",
                    Horario = "Lunes a Sábado: 8:00am a 8:00pm",
                    ImagenUrl = "/Content/Images/demo-construccion-1.jpg",
                    Categoria = "Construcción",
                    Coordenadas = new Coordenadas(25.554219, -103.445714),
                    Telefonos = new List<string>()
                    {
                        "8717010101",
                        "8717010102",
                    },
                    Servicios = new List<Servicio>()
                    {
                        new Servicio()
                        {
                            Id = "1",
                            Nombre = "Servicios de Construcción",
                            Descripcion = "Todo tipo de servicios de construcción de la mejor calidad.",
                        }
                    },
                    Testimonios = new List<Testimonio>()
                    {
                        new Testimonio()
                        {
                            Id = "1",
                            Nombre = "Ing. King Kong",
                            Descripcion = "Roar!",
                            ImagenUrl = "/Content/Images/persona-3.png"
                        }
                    }
                },
                new Resultado()
                {
                    Id = "3",
                    Nombre = "Proveedores Construcción",
                    Descripcion = "Venta de todo tipo de productos para construcción.",
                    Direccion = "Adelaida Chávez #33. Col. Mayagoitia. Lerdo, Dgo.",
                    Horario = "8:00am a 8:00pm",
                    ImagenUrl = "/Content/Images/demo-construccion-1.jpg",
                    Categoria = "Construcción",
                    Coordenadas = new Coordenadas(25.553919, -103.442216)
                },
                new Resultado()
                {
                    Id = "4",
                    Nombre = "Elctromax",
                    Descripcion = "Venta de productos para instalaciones eléctricas",
                    Direccion = "Adelaida Chávez #33. Col. Mayagoitia. Lerdo, Dgo.",
                    Horario = "8:00am a 8:00pm",
                    ImagenUrl = "/Content/Images/demo-electrico-1.jpg",
                    Categoria = "Electricidad",
                    Coordenadas = new Coordenadas(25.553919, -103.442216)
                },
                new Resultado()
                {
                    Id = "5",
                    Nombre = "Instalaciones Eléctricas del Norte",
                    Descripcion = "Instalaciones eléctricas residenciales, comerciales e industriales.",
                    Direccion = "Adelaida Chávez #33. Col. Mayagoitia. Lerdo, Dgo.",
                    Horario = "8:00am a 8:00pm",
                    ImagenUrl = "/Content/Images/demo-electrico-1.jpg",
                    Categoria = "Electricidad",
                    Coordenadas = new Coordenadas(25.553919, -103.442216)
                }
            };
        }

        public static IEnumerable<string> Categorias()
        {
            return new List<string>()
            {
                "Construcción",
                "Electricidad",
                "Plomeria",
                "Soldadura"
            };
        }

        public static IEnumerable<Ubicacion> Ubicaciones()
        {
            return new List<Ubicacion>()
            {
                new Ubicacion()
                {
                    Id = "1",
                    Ciudad = "Lerdo",
                    Estado = "Durango",
                    Abreviatura = "DGO"
                },
                new Ubicacion()
                {
                    Id = "2",
                    Ciudad = "Gómez Palacio",
                    Estado = "Durango",
                    Abreviatura = "DGO"
                },
                new Ubicacion()
                {
                    Id = "3",
                    Ciudad = "Torreón",
                    Estado = "Coahuila",
                    Abreviatura = "COAH"
                },
                new Ubicacion()
                {
                    Id = "4",
                    Ciudad = "Saltillo",
                    Estado = "Coahuila",
                    Abreviatura = "COAH"
                },
                new Ubicacion()
                {
                    Id = "5",
                    Ciudad = "Monterrey",
                    Estado = "Nuevo León",
                    Abreviatura = "NL"
                },
                new Ubicacion()
                {
                    Id = "6",
                    Ciudad = "Toluca de Lerdo",
                    Estado = "Estado de México",
                    Abreviatura = "EDOMEX"
                },
                new Ubicacion()
                {
                    Id = "7",
                    Ciudad = "Matamoros",
                    Estado = "Coahuila",
                    Abreviatura = "COAH"
                },
                new Ubicacion()
                {
                    Id = "8",
                    Ciudad = "Matamoros",
                    Estado = "Tamaulipas",
                    Abreviatura = "TAMPS"
                }
            };
        }
    }
}