﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mosses.Travertino.Web.Startup))]
namespace Mosses.Travertino.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
