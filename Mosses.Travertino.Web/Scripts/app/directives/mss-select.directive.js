﻿(function () {
    angular.module('DirApp').directive('mssSelect', mssSelect);

    function mssSelect() {
        var directive = {
            restrict: 'E',
            scope: {
                ngModel: '=',
                opciones: '='
            },
            template: '<select class="form-control" name="BusquedaUbicacion" id="busqueda-categoria" ' +
            'ng-model="ngModel" ng-options="opcion for opcion in opciones"> ' +
            '<option></option> ' +
            '</select>',
            link: link
        };

        return directive;

        function link(scope, element, attributes) {
            element.find('select:first').select2({
                placeholder: attributes.placeholder,
                allowClear: attributes.allowClear
            });
        }
    }
})();