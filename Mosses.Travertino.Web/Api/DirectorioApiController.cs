﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Mosses.Travertino.Common.Managers;
using Mosses.Travertino.Common.Models;

namespace Mosses.Travertino.Web.Api
{
    [RoutePrefix("api/directorio")]
    public class DirectorioApiController : ApiController
    {
        private IDirectorioManager DirectorioManager { get; set; }

        public DirectorioApiController(IDirectorioManager directorioManager)
        {
            DirectorioManager = directorioManager;
        }

        [HttpGet]
        [Route("sugerencias")]
        public IHttpActionResult GetSugerencias(string indicio)
        {
            try
            {
                IEnumerable<string> sugerencias = DirectorioManager.ObtenerSugerencias(indicio);
                return Ok(sugerencias);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("busqueda")]
        public IHttpActionResult GetBusqueda(string termino, string ubicacion)
        {
            try
            {
                IEnumerable<Resultado> resultados = DirectorioManager.Buscar(new BusquedaParams(termino, ubicacion));
                return Ok(resultados);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("ubicaciones")]
        public IHttpActionResult GetUbicaciones(string indicio)
        {
            try
            {
                IEnumerable<Ubicacion> ubicaciones = DirectorioManager.ObtenerUbicaciones(indicio);
                return Ok(ubicaciones);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("categorias")]
        public IHttpActionResult GetCategorias()
        {
            try
            {
                IEnumerable<string> categorias = DirectorioManager.ObtenerCategorias();
                return Ok(categorias);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
