﻿using System.Linq;
using System.Web.Mvc;
using Mosses.Travertino.Common.Managers;
using Mosses.Travertino.Common.Models;

namespace Mosses.Travertino.Web.Controllers
{
    public class DirectorioController : BaseController
    {
        private IDirectorioManager DirectorioManager { get; set; }

        public DirectorioController(IDirectorioManager directorioManager)
        {
            DirectorioManager = directorioManager;
        }

        [HttpGet]
        public ActionResult Index(string termino, string ubicacion)
        {
            if (!string.IsNullOrEmpty(termino) && !string.IsNullOrEmpty(ubicacion))
            {
                var busqueda = new BusquedaParams(termino, ubicacion);
                ViewBag.Resultados = DirectorioManager.Buscar(busqueda).ToList();
                ViewBag.BusquedaExterna = busqueda;
            }
            
            ViewBag.EsPrincipal = true;
            ViewBag.Categorias = DirectorioManager.ObtenerCategorias();

            return View();
        }

        [HttpGet]
        public ActionResult Detalle(string companyId)
        {
            if (string.IsNullOrEmpty(companyId))
                return RedirectToAction("Index", "Directorio");

            Resultado company = DirectorioManager.Obtener(companyId);
            return View(company);
        }
    }
}